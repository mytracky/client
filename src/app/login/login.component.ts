import { Component, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../service/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  @Output() loginStatus = new EventEmitter<boolean>();

  loginForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });

  get username() { return this.loginForm.value.username; }
  get password() { return this.loginForm.value.password; }

  constructor(public authService: AuthService) { }

  login() {
    this.authService
      .login(this.username, this.password)
      .subscribe(status => this.loginStatus.emit(status));
  }
}
