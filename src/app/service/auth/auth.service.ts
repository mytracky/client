import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

import { AppConstants } from '../../utils/constants';
import { LoginResponse } from '../../models/login-response';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public token: string;
  private apiURL: string;
  public redirectUrl: string;

  constructor(private http: HttpClient) {
    const currentUser = this.getUserFromLocalStorage();
    this.token = currentUser && currentUser.token;
    this.apiURL = AppConstants.apiURL();
  }

  login(username: string, password: string): Observable<boolean> {
    return this.http.post(this.apiURL + '/login', { username, password })
      .pipe(map((response: LoginResponse) => {
        console.log('server response', response);
        const token = response && response.token;
        if (token) {
          this.token = token;
          localStorage.setItem('currentUser', JSON.stringify({ username, token }));
          return true;
        } else {
          return false;
        }
    }));
  }

  logout(): void {
    this.token = null;
    localStorage.removeItem('currentUser');
  }

  isUserLoggedIn(): boolean {
    return this.getUserFromLocalStorage() ? true : false;
  }

  getUserFromLocalStorage() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  getUserToken() {
    return this.getUserFromLocalStorage() && this.getUserFromLocalStorage().token;
  }
}
