import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, retry, timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(private _http: HttpClient) {}

  getFromURL<T = any>(url: string, params?: { [propName: string]: any }, external = false) {
    return this._http.get<T>(url, { params, withCredentials: true }).pipe( );
  }

  private _errorHandler() {
    return (error: any, data: any) => {
      return throwError(`Timeout error ${error}`);
    };
  }
}
