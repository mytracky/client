import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Route,
  Router,
  RouterStateSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthService } from '../auth/auth.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(private _authService: AuthService, private _router: Router) {
    // _authService.onInitUser();
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this._authService.isUserLoggedIn()) {
      this._router.navigate(['/']);
      return false;
    }

    return true;
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    // return true;
    return this.checkLogin(state.url, 'login');
  }

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }

  checkLogin(url: string, navigateUrl: string) {
    if (!this._authService.isUserLoggedIn()) {
      this._navigateToUrl(url, navigateUrl);
    } else {
      return true;
    }
  }

  private _navigateToUrl(redirectUrl?: string, navigatedUrl?: string) {
    if (redirectUrl) {
      this._authService.redirectUrl = redirectUrl;
    }
    this._router.navigate([navigatedUrl]);
    return of(false);
  }
}
