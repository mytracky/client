import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor } from '@angular/common/http';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(private auth: AuthService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler) {
      const authToken =  this.auth.getUserToken();

      if (authToken) {
        const cloneReq = req.clone({
          headers: req.headers.set(`Authorization`, `Bearer_${authToken}`),
        });
        return next.handle(cloneReq);
      }
      return next.handle(req);
    }
}
