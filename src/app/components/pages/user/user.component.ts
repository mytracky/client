import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/service/http.service';
import { AppConstants } from 'src/app/utils/constants';
import { HttpClient } from '@angular/common/http';

interface User {
  username?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  public user;

  constructor(public http: HttpClient) { }

  ngOnInit() {
    this.http.get(AppConstants.apiURL() + '/users/44').subscribe(
      (user: User) => this.user = user);
  }

}
