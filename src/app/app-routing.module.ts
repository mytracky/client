import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './service/guard/auth.guard';
import { Error404Component } from './components/pages/error404/error404.component';
import { UserComponent } from './components/pages/user/user.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'user/:id',
        component: UserComponent,
        pathMatch: 'full'
      },
      {
        path: 'user/',
        component: UserComponent,
        pathMatch: 'full'
      }
    ]
  },
  { 
    path: 'login', 
    component: LoginComponent,
    pathMatch: 'full'
  },
  { path: '**', component: Error404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
