import { Component, OnInit } from '@angular/core';
import { AuthService } from './service/auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { AppConstants } from './utils/constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public isLogin = false;
  public user: string;

  constructor(public authService: AuthService, public http: HttpClient) { }

  ngOnInit() {
    this.isLogin = this.authService.isUserLoggedIn();
  }

  setLoginStatus(status) {
    this.isLogin = status;
  }
}
